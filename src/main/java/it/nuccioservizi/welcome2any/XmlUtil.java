package it.nuccioservizi.welcome2any;

import java.util.AbstractList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * https://stackoverflow.com/a/19591302/453605
 */
public final class XmlUtil {
    private XmlUtil() {
    }

    public static List<Node> asList(final NodeList n) {
        if (n.getLength() == 0) {
            final List<Node> emptyList = Collections.<Node>emptyList();
            if (emptyList == null)
                throw new RuntimeException("Empty list null");

            return emptyList;
        }

        return new StaticNodeListWrapper(n);
    }

    public static Map<String, String> nodeToMap(final Node node) {
        final Map<String, String> vals = new HashMap<>();
        final NodeList props = node.getChildNodes();
        for (int i = 0, ii = props.getLength(); i < ii; ++i) {
            final Node prop = props.item(i);

            final String nodeName = prop.getNodeName();
            if (nodeName == null)
                throw new RuntimeException("Nodo senza nome");

            final String nodeText = prop.getTextContent().trim();
            if (nodeText == null)
                throw new RuntimeException("Nodo senza testo");

            vals.put(nodeName, nodeText);
        }
        return vals;
    }

    /**
     * Consente di vedere una NodeList come una List. È statica perché memorizzo
     * la lunghezza senza aggiornarla.
     *
     * @author marcenuc
     *
     */
    static final class StaticNodeListWrapper extends AbstractList<Node>
            implements RandomAccess {
        private final NodeList list;
        private final int listSize;

        StaticNodeListWrapper(final NodeList l) {
            this.list = l;
            this.listSize = l.getLength();
        }

        @Override
        public Node get(final int index) {
            if (index < 0 || index >= this.listSize)
                throw new IllegalArgumentException(String.format(
                        "%i è un indice non valido. Valori ammessi da 0 a %i",
                        Integer.valueOf(index),
                        Integer.valueOf(this.listSize - 1)));

            final Node item = this.list.item(index);

            if (item == null)
                throw new RuntimeException("Item nulla all'indice " + index);

            return item;
        }

        @Override
        public int size() {
            return this.listSize;
        }
    }
}
