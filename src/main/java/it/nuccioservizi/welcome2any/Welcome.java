package it.nuccioservizi.welcome2any;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import it.nuccioservizi.welcome2any.g1.ImportExportG1;

public final class Welcome {
    @SuppressWarnings("null")
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyy");

    public static LocalDate parseLocalDate(final String welcomeDate) {
        @SuppressWarnings("null")
        @NonNull
        final LocalDate date = LocalDate.parse(welcomeDate, DATE_FORMATTER);

        return date;
    }

    public static Optional<LocalDate> parseOptionalLocalDate(
            final String dateValue) {
        if (dateValue.isEmpty()) {
            @SuppressWarnings("null")
            @NonNull
            final Optional<LocalDate> empty = Optional.empty();

            return empty;
        }

        final LocalDate date = parseLocalDate(dateValue);

        @SuppressWarnings("null")
        @NonNull
        final Optional<LocalDate> dateOptional = Optional.of(date);

        return dateOptional;
    }

    public static final String XPATH_ANTICIPO_GIORNO = "Anticipi/Anticipo";
    public static final String XPATH_FATTURA_GIORNO = "FattureEmesse/FatturaEmessa";
    public static final String XPATH_DATA = "Data";
    public static final String XPATH_GIORNO = "/Corrispettivi/Giorno";
    public static final String XPATH_CODICE = "Codice";
    public static final String XPATH_CLIENTE = "/Corrispettivi/Clienti/Cliente";
    public static final String XPATH_CODICE_CLIENTE = "CodiceCliente";
    public static final String XPATH_CAUSALE = "Causale";
    public static final String XPATH_FATTURA = "/Corrispettivi/Giorno/FattureEmesse/FatturaEmessa";

    /**
     * TODO: eliminare la classe omonima in {@link ImportExportG1}
     */
    public static final class TipoFattura {
        public final String suffisso;
        public final String causale;
        public final int attivitàIva;
        public final int sezionaleIva;

        public TipoFattura(final String suffisso, final String causale, final int attivitàIva,
                final int sezionaleIva) {
            this.suffisso = suffisso;
            this.causale = causale;
            this.attivitàIva = attivitàIva;
            this.sezionaleIva = sezionaleIva;
        }
    }

    private final TipoFattura fattura;
    private final TipoFattura notaDiCredito;

    public Welcome(final TipoFattura fattura, final TipoFattura notaDiCredito) {
        this.fattura = fattura;
        this.notaDiCredito = notaDiCredito;
    }

    public TipoFattura toTipoFattura(final boolean isNotaDiCredito)
            throws Exception {
        if (isNotaDiCredito)
            return notaDiCredito;

        return fattura;
    }

}
