package it.nuccioservizi.welcome2any;

import java.nio.file.Path;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.jdt.annotation.NonNull;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class XPathEasy {
    private final XPath xPath;
    private final Document document;

    public static XPath newXPath() {
        final XPathFactory xPathFactory = XPathFactory.newInstance();
        @SuppressWarnings("null")
        @NonNull
        final XPath xPath = xPathFactory.newXPath();

        return xPath;
    }

    public static XPathEasy fromFile(final XPath xPath,
            final DocumentBuilder documentBuilder, final Path xmlInput)
            throws Exception {
        @SuppressWarnings("null")
        @NonNull
        final Document welcomeDocument = documentBuilder
                .parse(xmlInput.toFile());

        return new XPathEasy(xPath, welcomeDocument);
    }

    public XPathEasy(final XPath xPath, final Document document) {
        this.xPath = xPath;
        this.document = document;
    }

    public String getNodeText(final Node context, final String expression)
            throws Exception {
        final String evaluate = this.xPath.evaluate(expression, context);

        if (evaluate == null)
            throw new IllegalStateException(
                    "L'espressione '" + expression + "' ha restituito null.");

        return evaluate;
    }

    public int getNodeInt(final Node context, final String expression)
            throws Exception {
        return Integer.parseInt(getNodeText(context, expression));
    }

    public int getNodeCent(final Node context, final String expression)
            throws Exception {
        return Integer.parseInt(getNodeText(context, expression)
                .replaceFirst("\\.(\\d\\d)$", "$1")); //$NON-NLS-1$ //$NON-NLS-2$
    }

    public List<Node> getNodes(final String expression)
            throws XPathExpressionException {
        final NodeList nodes = (NodeList) this.xPath.evaluate(expression,
                this.document, XPathConstants.NODESET);

        if (nodes == null)
            throw new IllegalStateException(
                    "L'espressione '" + expression + "' ha restituito null.");

        return XmlUtil.asList(nodes);
    }

    public List<Node> getNodes(final Node context, final String expression)
            throws XPathExpressionException {
        final NodeList nodes = (NodeList) this.xPath.evaluate(expression,
                context, XPathConstants.NODESET);

        if (nodes == null)
            throw new IllegalStateException(
                    "L'espressione '" + expression + "' ha restituito null.");

        return XmlUtil.asList(nodes);
    }
}
