package it.nuccioservizi.welcome2any.g1;

import org.eclipse.jdt.annotation.NonNull;

public class Clienti {
    final String CODCLI;
    final String RAGSOC;
    final String INDIR;
    final String CAP;
    final String LOCAL;
    final String PROV;
    final String CODFISC;
    final String PARTIVA;
    final String CODPAG;
    final String IVA;
    final String SCONTO1;
    final String SCONTO2;
    final String RAGGRDOC;
    final String CONTOPDC;
    final String PERSOC;
    final String PREZZO;
    final String ADDSPESE;
    final String MASTRO;
    final String CODNAZ;
    final String CLASSE;
    final String ALIAS;
    final String PROVN;
    final String DTNASCPF;
    final String NOMEPF;
    final String COGNPF;

    private static final String truncate(final String value, final int length) {
        @SuppressWarnings("null")
        @NonNull
        final String s = value.substring(0, Math.min(length, value.length()));

        return s;
    }

    Clienti(final String cODCLI, final String rAGSOC, final String iNDIR,
            final String cAP, final String lOCAL, final String pROV,
            final String cODFISC, final String pARTIVA, final String cODPAG,
            final String iVA, final String sCONTO1, final String sCONTO2,
            final String rAGGRDOC, final String cONTOPDC, final String pERSOC,
            final String pREZZO, final String aDDSPESE, final String mASTRO,
            final String cODNAZ, final String cLASSE, final String aLIAS,
            final String pROVN, final String dTNASCPF, final String nOMEPF,
            final String cOGNPF) {
        this.CODCLI = cODCLI;
        this.RAGSOC = truncate(rAGSOC, 60);
        this.INDIR = truncate(iNDIR, 80);
        this.CAP = cAP;
        this.LOCAL = truncate(lOCAL, 50);
        this.PROV = pROV;
        this.CODFISC = cODFISC;
        this.PARTIVA = pARTIVA;
        this.CODPAG = cODPAG;
        this.IVA = iVA;
        this.SCONTO1 = sCONTO1;
        this.SCONTO2 = sCONTO2;
        this.RAGGRDOC = rAGGRDOC;
        this.CONTOPDC = cONTOPDC;
        this.PERSOC = pERSOC;
        this.PREZZO = pREZZO;
        this.ADDSPESE = aDDSPESE;
        this.MASTRO = mASTRO;
        this.CODNAZ = cODNAZ;
        this.CLASSE = cLASSE;
        this.ALIAS = aLIAS;
        this.PROVN = pROVN;
        this.DTNASCPF = dTNASCPF;
        this.NOMEPF = truncate(nOMEPF, 30);
        this.COGNPF = truncate(cOGNPF, 30);
    }
}
