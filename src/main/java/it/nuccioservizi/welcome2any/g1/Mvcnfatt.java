package it.nuccioservizi.welcome2any.g1;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Node;

import it.nuccioservizi.welcome2any.XPathEasy;

public class Mvcnfatt {
    final int ANNREG;
    final int NUMREG;
    final int NUMRIG;
    final String DATREG;
    @Nullable
    final Integer ANNIVA;
    final String DATCOM;
    final String CAUSALE;
    final String CAMBIO;
    final String RIFCAMBIO;
    final String IMPVAL;
    final int NUMPRO;
    final String NUMDOC;
    final String DATDOC;
    final String CONTO;
    final String IMPORTO;
    final String SEGNO;
    final String CONTROP;
    final String DESCSTD;
    @Nullable
    final String IVAIND;
    @Nullable
    final String TIPOIVA;
    @Nullable
    final String ALIQIVA;
    final String REGIVA;
    final String IMPONIB;
    final String STATOCON;
    final String STATOIVA;
    final String TIPOMOV;
    final String RIGAGIO;
    final String PAGGIO;
    final String DATCOMRG;
    @Nullable
    final String CODPAG;
    final String DTINICOMP;
    final String DTFINCOMP;

    private Mvcnfatt(final int aNNREG, final int nUMREG, final int nUMRIG,
            final String dATREG, @Nullable final Integer aNNIVA,
            final String dATCOM, final String cAUSALE, final String cAMBIO,
            final String rIFCAMBIO, final String iMPVAL, final int nUMPRO,
            final String nUMDOC, final String dATDOC, final String cONTO,
            final String iMPORTO, final String sEGNO, final String cONTROP,
            final String dESCSTD, @Nullable final String iVAIND,
            @Nullable final String tIPOIVA, @Nullable final String aLIQIVA,
            final String rEGIVA, final String iMPONIB, final String sTATOCON,
            final String sTATOIVA, final String tIPOMOV, final String rIGAGIO,
            final String pAGGIO, final String dATCOMRG,
            @Nullable final String cODPAG, final String dTINICOMP,
            final String dTFINCOMP) {
        this.ANNREG = aNNREG;
        this.NUMREG = nUMREG;
        this.NUMRIG = nUMRIG;
        this.DATREG = dATREG;
        this.ANNIVA = aNNIVA;
        this.DATCOM = dATCOM;
        this.CAUSALE = cAUSALE;
        this.CAMBIO = cAMBIO;
        this.RIFCAMBIO = rIFCAMBIO;
        this.IMPVAL = iMPVAL;
        this.NUMPRO = nUMPRO;
        this.NUMDOC = nUMDOC;
        this.DATDOC = dATDOC;
        this.CONTO = cONTO;
        this.IMPORTO = iMPORTO;
        this.SEGNO = sEGNO;
        this.CONTROP = cONTROP;
        this.DESCSTD = dESCSTD;
        this.IVAIND = iVAIND;
        this.TIPOIVA = tIPOIVA;
        this.ALIQIVA = aLIQIVA;
        this.REGIVA = rEGIVA;
        this.IMPONIB = iMPONIB;
        this.STATOCON = sTATOCON;
        this.STATOIVA = sTATOIVA;
        this.TIPOMOV = tIPOMOV;
        this.RIGAGIO = rIGAGIO;
        this.PAGGIO = pAGGIO;
        this.DATCOMRG = dATCOMRG;
        this.CODPAG = cODPAG;
        this.DTINICOMP = dTINICOMP;
        this.DTFINCOMP = dTFINCOMP;
    }

    public static Mvcnfatt riga1(final boolean notaDiCredito,
            final String causale, final String data, final int anno,
            final int numreg, final int numpro, final String numdoc,
            final String conto, final String controp, final String descstd,
            final String regiva, final int totDocumento) {
        final int numrig = 1;
        final int importo = Math.abs(totDocumento);

        return new Mvcnfatt( //
                anno, // ANNREG *
                numreg, // NUMREG *
                numrig, // NUMRIG *
                data, // DATREG *
                null, // ANNIVA
                data, // DATCOM
                causale, // CAUSALE *
                "0", // CAMBIO
                "E", // RIFCAMBIO
                "0", // IMPVAL
                numpro, // NUMPRO
                numdoc, // NUMDOC *
                data, // DATDOC *
                conto, // CONTO
                ImportExportG1.daCentAValoreConCentesimi(importo), // IMPORTO
                segno(!notaDiCredito), // SEGNO
                controp, // CONTROP
                descstd, // DESCSTD *
                null, // IVAIND
                null, // TIPOIVA
                null, // ALIQIVA
                regiva, // REGIVA *
                "0", // IMPONIB
                "1", // STATOCON
                "0", // STATOIVA
                toTipomov(notaDiCredito), // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                data, // DATCOMRG
                "RD01", // CODPAG,
                data, // DTINICOMP
                data // DTFINCOMP
        );
    }

    public static Mvcnfatt rigaContabile(final XPathEasy in,
            final boolean notaDiCredito, final String causale,
            final String data, final int anno, final int numreg,
            final int numpro, final String numdoc, final int numrig,
            final String controp, final String descstd, final String regiva,
            final Node contropartita) throws Exception {
        final int importo = in.getNodeCent(contropartita, "Importo");
        final String importoSenzaSegno = ImportExportG1
                .daCentAValoreConCentesimi(importo < 0 ? -importo : importo);

        return new Mvcnfatt( //
                anno, // ANNREG *
                numreg, // NUMREG
                numrig, // NUMRIG *
                data, // DATREG *
                null, // ANNIVA
                data, // DATCOM
                causale, // CAUSALE *
                "0", // CAMBIO
                "E", // RIFCAMBIO
                "0", // IMPVAL
                numpro, // NUMPRO
                numdoc, // NUMDOC *
                data, // DATDOC *
                in.getNodeText(contropartita, "Codice"), // CONTO
                importoSenzaSegno, // IMPORTO
                segno(notaDiCredito), // SEGNO
                controp, // CONTROP
                descstd, // DESCSTD *
                null, // IVAIND
                null, // TIPOIVA
                in.getNodeText(contropartita, "IvaNetto/Codice"), // ALIQIVA
                regiva, // REGIVA *
                "0", // IMPONIB
                "1", // STATOCON
                "0", // STATOIVA
                toTipomov(notaDiCredito), // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                data, // DATCOMRG
                null, // CODPAG,
                data, // DTINICOMP
                data // DTFINCOMP
        );
    }

    private static String segno(final boolean notaDiCredito) {
        return notaDiCredito ? G1.DARE : G1.AVERE;
    }

    private static String toTipomov(final boolean notaDiCredito) {
        return notaDiCredito ? "2" : "1";
    }

    public static Mvcnfatt rigaIva(final XPathEasy in,
            final boolean notaDiCredito, final String causale,
            final String data, final int anno, final int numreg,
            final int numpro, final String numdoc, final int numrig,
            final String conto, final String controp, final String descstd,
            final String regiva, final Node contropartita) throws Exception {
        final int imponibile = in.getNodeCent(contropartita,
                "IvaNetto/Imponibile");
        final int imposta = in.getNodeCent(contropartita, "IvaNetto/Imposta");
        final String imponibileSenzaSegno = ImportExportG1
                .daCentAValoreConCentesimi(Math.abs(imponibile));
        final String impostaSenzaSegno = ImportExportG1
                .daCentAValoreConCentesimi(Math.abs(imposta));

        return new Mvcnfatt( //
                anno, // ANNREG *
                numreg, // NUMREG
                numrig, // NUMRIG *
                data, // DATREG *
                Integer.valueOf(anno), // ANNIVA
                data, // DATCOM
                causale, // CAUSALE *
                "0", // CAMBIO
                "E", // RIFCAMBIO
                "0", // IMPVAL
                numpro, // NUMPRO
                numdoc, // NUMDOC *
                data, // DATDOC *
                conto, // CONTO
                impostaSenzaSegno, // IMPORTO
                segno(notaDiCredito), // SEGNO
                controp, // CONTROP
                descstd, // DESCSTD *
                "N", // IVAIND
                "N", // TIPOIVA
                in.getNodeText(contropartita, "IvaNetto/Codice"), // ALIQIVA
                regiva, // REGIVA *
                imponibileSenzaSegno, // IMPONIB
                "0", // STATOCON
                "1", // STATOIVA
                toTipomov(notaDiCredito), // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                data, // DATCOMRG
                null, // CODPAG,
                data, // DTINICOMP
                data // DTFINCOMP
        );
    }

}
