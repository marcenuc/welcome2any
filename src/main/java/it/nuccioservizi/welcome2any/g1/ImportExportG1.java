package it.nuccioservizi.welcome2any.g1;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Node;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import it.nuccioservizi.welcome2any.U;
import it.nuccioservizi.welcome2any.Welcome;
import it.nuccioservizi.welcome2any.XPathEasy;
import it.nuccioservizi.welcome2any.XmlUtil;
import net.iryndin.jdbf.core.DbfRecord;
import net.iryndin.jdbf.reader.DbfReader;

public final class ImportExportG1 {
    @SuppressWarnings("null")
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter
            .ofPattern("dd/MM/uuuu");

    private int numDocumenti = 0;
    private final List<Clienti> clienti = new ArrayList<>();
    private final List<Mvcnfatt> mvcnfatts = new ArrayList<>();
    private int numPrimeNote = 0;
    private final List<Mvcnprim> mvcnprims = new ArrayList<>();
    private final List<Mvcncorr> mvcncorrs = new ArrayList<>();
    private final CacheClienti cacheClienti;
    private final Cfg cfg;
    private final TipoFattura fattura;
    private final TipoFattura notaDiCredito;
    private final TipoFattura fatturaLocazione;

    public void scriviXml(final OutputStream xmlG1) {
        final XStream xstream = new XStream(
                new DomDriver(StandardCharsets.UTF_8.name()));

        xstream.omitField(ImportExportG1.class, "numDocumenti");
        xstream.addImplicitCollection(ImportExportG1.class, "clienti");
        xstream.addImplicitCollection(ImportExportG1.class, "mvcnfatts");
        xstream.omitField(ImportExportG1.class, "numPrimeNote");
        xstream.addImplicitCollection(ImportExportG1.class, "mvcnprims");
        xstream.addImplicitCollection(ImportExportG1.class, "mvcncorrs");
        xstream.omitField(ImportExportG1.class, "cacheClienti");
        xstream.omitField(ImportExportG1.class, "cfg");
        xstream.omitField(ImportExportG1.class, "fattura");
        xstream.omitField(ImportExportG1.class, "notaDiCredito");
        xstream.omitField(ImportExportG1.class, "fatturaLocazione");

        aliasClass(xstream, ImportExportG1.class);
        aliasClass(xstream, Clienti.class);
        aliasClass(xstream, Mvcnfatt.class);
        aliasClass(xstream, Mvcnprim.class);
        aliasClass(xstream, Mvcncorr.class);

        xstream.toXML(this, xmlG1);
    }

    private static <T> String classToTagG1(final Class<T> cls) {
        @SuppressWarnings("null")
        @NonNull
        final String name = cls.getSimpleName();

        if (cls == ImportExportG1.class)
            return name;

        @SuppressWarnings("null")
        @NonNull
        final String upperCaseName = name.toUpperCase();

        return upperCaseName;
    }

    private static <T> void aliasClass(final XStream xstream,
            final Class<T> cls) {
        xstream.alias(classToTagG1(cls), cls);
    }

    static final class TipoFattura {
        final String suffisso;
        final String causale;
        final String regiva;

        TipoFattura(final String suffisso, final String causale,
                final String regiva) {
            this.suffisso = suffisso;
            this.causale = causale;
            this.regiva = regiva;
        }
    }

    private TipoFattura toTipoFattura(final XPathEasy in,
            final boolean isNotaDiCredito, final List<Node> contropartite)
            throws Exception {
        if (isNotaDiCredito)
            return this.notaDiCredito;

        @SuppressWarnings("null")
        final Node primaContropartita = contropartite.get(0);
        final String contoPrimaContropartita = in
                .getNodeText(primaContropartita, "Codice");

        if (this.cfg.conto_di_ricavo_fattura_di_locazione
                .equals(contoPrimaContropartita)) {
            return this.fatturaLocazione;
        }

        return this.fattura;
    }

    public void addFatturaONotaDiCredito(final XPathEasy in,
            final LocalDate dataDocumento, final Node docFattura,
            final boolean isNotaDiCredito) throws Exception {
        final List<Node> contropartite = in.getNodes(docFattura,
                "Contropartite/Contropartita");
        final TipoFattura tipoFattura = toTipoFattura(in, isNotaDiCredito,
                contropartite);
        final int anno = dataDocumento.getYear();
        final String data = format(dataDocumento);
        final int numdocW = in.getNodeInt(docFattura, "Numero");
        final String numdoc = numdocW + "/" + tipoFattura.suffisso;
        final String desc = numdoc + " " + data;
        final int totDocumento = in.getNodeCent(docFattura, "TotaleDocumento");

        final String conto = this.cfg.cod_mastro_clienti + this.cacheClienti
                .codcli(in.getNodeText(docFattura, "CodiceCliente"));

        ++this.numDocumenti;
        int numrig = 1;

        for (final Node contropartita : contropartite) {
            if (numrig == 1)
                this.mvcnfatts
                        .add(Mvcnfatt.riga1(isNotaDiCredito, tipoFattura.causale,
                                data, anno, this.numDocumenti, numdocW, numdoc,
                                conto, in.getNodeText(contropartita, "Codice"),
                                desc, tipoFattura.regiva, totDocumento));

            this.mvcnfatts.add(Mvcnfatt.rigaContabile(in, isNotaDiCredito,
                    tipoFattura.causale, data, anno, this.numDocumenti, numdocW,
                    numdoc, ++numrig, conto, desc, tipoFattura.regiva,
                    contropartita));
        }

        final String contoIva = this.cfg.conto_iva_vendite;
        for (final Node contropartita : contropartite)
            this.mvcnfatts.add(Mvcnfatt.rigaIva(in, isNotaDiCredito,
                    tipoFattura.causale, data, anno, this.numDocumenti, numdocW,
                    numdoc, ++numrig, contoIva, conto, desc, tipoFattura.regiva,
                    contropartita));

        final List<Node> pagamenti = in.getNodes(docFattura, "Pagamento");

        if (!pagamenti.isEmpty())
            ++this.numPrimeNote;

        int numRighePrimaNota = 1;

        for (final Node pagamento : pagamenti) {
            final String causaleIncasso = isNotaDiCredito
                    ? this.cfg.causale_incasso_nota_di_credito
                    : this.cfg.causale_incasso_fattura;
            final int importo = in.getNodeCent(pagamento, "Importo");
            this.mvcnprims.add(Mvcnprim.pagamento(causaleIncasso, data, anno,
                    this.numPrimeNote, numRighePrimaNota, conto, desc, G1.AVERE,
                    importo));
            numRighePrimaNota++;
            final String contoDare = in.getNodeText(pagamento, "Codice");
            this.mvcnprims.add(Mvcnprim.pagamento(causaleIncasso, data, anno,
                    this.numPrimeNote, numRighePrimaNota++, contoDare, desc,
                    G1.DARE, importo));
        }
    }

    private static String format(final LocalDate data) {
        @SuppressWarnings("null")
        @NonNull
        final String out = data.format(DATE_FORMATTER);
        return out;
    }

    public void addRicevutaFiscale(final XPathEasy in,
            final LocalDate dataDocumento, final Node ricevuta)
            throws Exception {
        final int anno = dataDocumento.getYear();
        final String data = format(dataDocumento);

        final int numdocW = in.getNodeInt(ricevuta, "Numero");
        final String numdoc = numdocW + "/"
                + this.cfg.suffisso_num_documento_ricevuta_fiscale;
        final String desc = numdoc + " " + data;
        final String regiva = this.cfg.registro_iva_ricevuta_fiscale;

        ++this.numDocumenti;

        int numrig = 0;

        for (final Node contropartita : in.getNodes(ricevuta,
                "Contropartite/Contropartita")) {
            ++numrig;
            this.mvcncorrs.add(Mvcncorr.riga1(in, regiva, anno,
                    this.cfg.causale_ricevuta_fiscale,
                    this.cfg.conto_ricevuta_fiscale, this.numDocumenti, numrig,
                    data, desc, contropartita));
            this.mvcncorrs.add(Mvcncorr.riga2(in, regiva, anno,
                    this.cfg.causale_ricevuta_fiscale,
                    this.cfg.conto_ricevuta_fiscale, this.numDocumenti, numrig,
                    data, desc, contropartita));
            this.mvcncorrs.add(Mvcncorr.riga3(in, regiva, anno,
                    this.cfg.causale_ricevuta_fiscale,
                    this.cfg.conto_ricevuta_fiscale, this.numDocumenti, numrig,
                    data, desc, contropartita, this.cfg.conto_iva_vendite));
        }

        final List<Node> pagamenti = in.getNodes(ricevuta, "Pagamento");

        final boolean conPagamenti = !pagamenti.isEmpty();

        if (conPagamenti)
            ++this.numPrimeNote;

        int numRighePrimaNota = 1;

        for (final Node pagamentoWelcome : pagamenti) {
            final int importo = in.getNodeCent(pagamentoWelcome, "Importo");
            this.mvcnprims.add(Mvcnprim.pagamento(
                    this.cfg.causale_incasso_ricevuta_fiscale, data, anno,
                    this.numPrimeNote, numRighePrimaNota++, //
                    in.getNodeText(pagamentoWelcome, "Codice"), desc, G1.DARE,
                    importo));
            this.mvcnprims.add(Mvcnprim.pagamento(
                    this.cfg.causale_incasso_ricevuta_fiscale, data, anno, //
                    this.numPrimeNote, numRighePrimaNota++, //
                    this.cfg.conto_ricevuta_fiscale, desc, G1.AVERE, importo));
        }
    }

    public void addInserimentoCaparra(final XPathEasy in,
            final LocalDate dataDocumento, final Node anticipo)
            throws Exception {
        final int anno = dataDocumento.getYear();
        final String data = format(dataDocumento);

        final String desc = in.getNodeText(anticipo, "Descrizione");

        ++this.numPrimeNote;
        int numRighePrimaNota = 1;

        final String contoPagamento = in.getNodeText(anticipo,
                "ContoPagamento");
        final String contoCaparra = in.getNodeText(anticipo, "ContoAnticipo");
        final int importo = in.getNodeCent(anticipo, "Importo");

        this.mvcnprims.add(Mvcnprim.pagamento(this.cfg.causale_incasso_caparra,
                data, anno, this.numPrimeNote, numRighePrimaNota++, //
                contoPagamento, desc, G1.DARE, importo));

        this.mvcnprims.add(
                Mvcnprim.pagamento(this.cfg.causale_incasso_caparra, data, anno, //
                        this.numPrimeNote, numRighePrimaNota++, //
                        contoCaparra, desc, G1.AVERE, importo));
    }

    // Le istanze di questa classe devono essere create col metodo of().
    private ImportExportG1(final Cfg cfg, final CacheClienti cacheClienti,
            final TipoFattura fattura, final TipoFattura notaDiCredito,
            final TipoFattura fatturaLocazione) {
        this.cfg = cfg;
        this.cacheClienti = cacheClienti;
        this.fattura = fattura;
        this.notaDiCredito = notaDiCredito;
        this.fatturaLocazione = fatturaLocazione;
    }

    public void addCliente(final Node clienteWelcome) {
        this.clienti.add(newClienteFromNode(clienteWelcome));
    }

    public static ImportExportG1 of(final Cfg cfg) throws IOException {
        return new ImportExportG1(cfg,
                CacheClienti.caricaClientiG1(cfg.file_anagrafica_clienti_g1),
                new TipoFattura( //
                        cfg.suffisso_num_documento_fattura, cfg.causale_fattura, //
                        cfg.registro_iva_fattura),
                new TipoFattura( //
                        cfg.suffisso_num_documento_nota_di_credito,
                        cfg.causale_nota_di_credito, //
                        cfg.registro_iva_nota_di_credito),
                new TipoFattura( //
                        cfg.suffisso_num_documento_fattura_di_locazione,
                        cfg.causale_fattura_di_locazione,
                        cfg.registro_iva_fattura_di_locazione));
    }

    private static int daValoreConCentesimiACent(
            final String valoreConCentesimi) {
        return Integer.parseInt(
                valoreConCentesimi.replace(".", "").replaceFirst(",", ""));
    }

    static String daCentAValoreConCentesimi(final int cent) {
        final String s = Integer.toString(cent);
        final int indiceVirgola = s.length() - 2;

        if (s.length() == 1)
            return "0,0" + s;
        else if (s.length() == 2)
            return "0," + s;
        else
            return s.substring(0, indiceVirgola) + ","
                    + s.substring(indiceVirgola);
    }

    static String sommaEuro(final String a, final String b) {
        return daCentAValoreConCentesimi(
                daValoreConCentesimiACent(a) + daValoreConCentesimiACent(b));
    }

    private Clienti newClienteFromNode(final Node cliente) {
        final Map<String, String> vals = XmlUtil.nodeToMap(cliente);
        @Nullable
        final String denominazione = vals.get("RagioneSociale");
        @Nullable
        final String cognome = vals.get("Cognome");
        @Nullable
        final String nome = vals.get("Nome");
        @Nullable
        final String codiceFiscale = vals.get("CodiceFiscale");
        @Nullable
        final String codiceDaGestionale = vals.get("Codice");

        @Nullable
        final String indirizzo = vals.get("Indirizzo");
        @Nullable
        final String località = vals.get("Localita");
        @Nullable
        final String provincia = vals.get("Provincia");
        @Nullable
        final String capComune = vals.get("Cap");
        final String partitaIva = U.filtraCF(vals.get("PartitaIVA"));
        @SuppressWarnings("null")
        @NonNull
        final String dataDiNascita = Welcome
                .parseOptionalLocalDate(vals.get("DataNascita"))
                .map(DATE_FORMATTER::format).orElse("");
        @Nullable
        final String codIsoEstero = vals.get("SiglaNazione");

        final String codcli = this.cacheClienti.trovaOAssegnaCodCliente(
                codiceDaGestionale, codiceFiscale, partitaIva);

        return new Clienti( //
                codcli, // CODCLI;
                denominazione, // RAGSOC;
                indirizzo, // INDIR;
                capComune, // CAP;
                località, // LOCAL;
                provincia, // PROV;
                codiceFiscale, // CODFISC;
                partitaIva, // PARTIVA;
                "RD01", // CODPAG;
                "1000", // IVA;
                "0", // SCONTO1;
                "0", // SCONTO2;
                "S", // RAGGRDOC;
                this.cfg.conto_di_ricavo_ricevute_generiche, // CONTOPDC;
                "", // PERSOC;
                "1", // PREZZO;
                "N", // ADDSPESE;
                this.cfg.cod_mastro_clienti, // MASTRO;
                codIsoEstero, // CODNAZ;
                "C", // CLASSE;
                codiceDaGestionale, // ALIAS;
                "", // PROVN;
                dataDiNascita, // DTNASCPF;
                nome, // NOMEPF;
                cognome // COGNPF;
        );
    }

    public static void main(final String[] argv) throws IOException {
        final String anacfDbf = argv.length > 0 ? argv[0] : "ANACF.DBF";
        System.out.println("Dump struttura di " + anacfDbf);
        try (final DbfReader reader = new DbfReader(new File(anacfDbf))) {
            System.out.println(reader.getMetadata());
        }
    }

    private static final class CacheClienti {
        /*
         * N.B.: Eseguire il main di ImportExportG1 per stampare la lista e gli
         * indici dei campi nel file ANACF.DBF.
         */
        private static final String ANACF_CLFR = "CLFR";
        private static final String ANACF_CODCF = "CODCF";
        private static final String ANACF_ALIAS = "ALIAS";
        private static final String ANACF_CODFISC = "CODFISC";
        private static final String ANACF_PARTIVA = "PARTIVA";

        @SuppressWarnings("null")
        private static final Charset DBF_CHARSET = StandardCharsets.ISO_8859_1;

        private static final class Indice {
            private final Map<String, String> indice = new HashMap<>();

            Indice() {
                // niente da inizializzare
            }

            private static String normalizzaChiave(final String chiave) {
                @SuppressWarnings("null")
                @NonNull
                final String normalizzata = chiave.trim().toUpperCase();

                return normalizzata;
            }

            void aggiungi(final String chiave, final String valore) {
                if (!chiave.isEmpty() && !valore.isEmpty())
                    this.indice.put(normalizzaChiave(chiave), valore);
            }

            @SuppressWarnings("null")
            String cerca(final String chiave) {
                @Nullable
                final String valore = this.indice.get(normalizzaChiave(chiave));

                return valore == null ? "" : valore;
            }
        }

        private int ultimoCodCli = 0;
        private final Indice codiciClienti = new Indice();
        private final Indice codiciFiscali = new Indice();
        private final Indice partiteIva = new Indice();

        private static String getDbfField(final DbfRecord rec,
                final String fieldName) {
            final String nullableString = rec.getString(fieldName, DBF_CHARSET);

            return nullableString == null ? "" : nullableString;
        }

        private static boolean isCliente(final DbfRecord rec) {
            return "C".equals(getDbfField(rec, ANACF_CLFR));
        }

        private void aggiornaUltimoCodCli(final String codcf) {
            this.ultimoCodCli = Integer.max(Integer.parseInt(codcf),
                    this.ultimoCodCli);
        }

        private void ricordaAlias(final String alias, final String codcli) {
            this.codiciClienti.aggiungi(alias, codcli);
        }

        private void ricordaCodFiscale(final String codfisc,
                final String codcf) {
            this.codiciFiscali.aggiungi(codfisc, codcf);
        }

        private void ricordaPartitaIva(final String partiva,
                final String codcf) {
            this.partiteIva.aggiungi(partiva, codcf);
        }

        static CacheClienti caricaClientiG1(final Path pathAnacfDbf)
                throws IOException {
            final CacheClienti cacheClienti = new CacheClienti();

            try (final DbfReader reader = new DbfReader(
                    pathAnacfDbf.toFile())) {
                for (DbfRecord rec = reader.read(); rec != null; rec = reader
                        .read())
                    if (!rec.isDeleted() && isCliente(rec)) {
                        final String codcf = getDbfField(rec, ANACF_CODCF);

                        cacheClienti.aggiornaUltimoCodCli(codcf);

                        if (isCliente(rec)) {
                            cacheClienti.ricordaAlias(
                                    getDbfField(rec, ANACF_ALIAS), codcf);
                            cacheClienti.ricordaCodFiscale(
                                    getDbfField(rec, ANACF_CODFISC), codcf);
                            cacheClienti.ricordaPartitaIva(
                                    getDbfField(rec, ANACF_PARTIVA), codcf);
                        }
                    }
            }
            return cacheClienti;
        }

        String codcli(final String codClienteWelcome) {
            final String cc = this.codiciClienti.cerca(codClienteWelcome);

            if (cc.isEmpty())
                throw new IllegalStateException(
                        "Cliente Welcome senza codice G1: "
                                + codClienteWelcome);

            return cc;
        }

        @SuppressWarnings("null")
        String trovaOAssegnaCodCliente(final String codClienteWelcome,
                final String codfisc, final String partiva) {
            if (codClienteWelcome.isEmpty())
                throw new IllegalStateException("Codice Cliente vuoto.");

            String codClienteG1 = this.codiciClienti.cerca(codClienteWelcome);
            if (codClienteG1.isEmpty()) {
                codClienteG1 = this.codiciFiscali.cerca(codfisc);
                if (codClienteG1.isEmpty()) {
                    codClienteG1 = this.partiteIva.cerca(partiva);
                    if (codClienteG1.isEmpty())
                        codClienteG1 = String.valueOf(++this.ultimoCodCli);
                }

                /*
                 * Se abbiamo trovato un cliente tramite CF o PIVA, ricordiamolo
                 * col codice Welcome per trovarlo durante l'inserimento delle
                 * fatture dove abbiamo solo quello.
                 */
                ricordaAlias(codClienteWelcome, codClienteG1);
            }

            return codClienteG1;
        }
    }
}
