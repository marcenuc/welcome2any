package it.nuccioservizi.welcome2any.g1;

import org.w3c.dom.Node;

import it.nuccioservizi.welcome2any.XPathEasy;

public class Mvcncorr {
    final int ANNREG; // *
    final int NUMREG; // *
    final int NUMRIG; // *
    final String DATREG; // *
    final int ANNIVA;
    final String CAUSALE; // *
    final String CONTO;
    final String IMPORTO;
    final String SEGNO;
    final String CONTROP;
    final String DESC; // *
    final String IVAIND;
    final String TIPOIVA;
    final String ALIQIVA;
    final String REGIVA; // *
    final String IMPONIB;
    final String STATOCON;
    final String STATOIVA;
    final String TIPOMOV; // *
    final String RIGAGIO;
    final String PAGGIO;
    final String CONCNTRP;

    private Mvcncorr(final int aNNREG, final int nUMREG, final int nUMRIG,
            final String dATREG, final int aNNIVA, final String cAUSALE,
            final String cONTO, final String iMPORTO, final String sEGNO,
            final String cONTROP, final String dESC, final String iVAIND,
            final String tIPOIVA, final String aLIQIVA, final String rEGIVA,
            final String iMPONIB, final String sTATOCON, final String sTATOIVA,
            final String tIPOMOV, final String rIGAGIO, final String pAGGIO,
            final String cONCNTRP) {
        super();
        this.ANNREG = aNNREG;
        this.NUMREG = nUMREG;
        this.NUMRIG = nUMRIG;
        this.DATREG = dATREG;
        this.ANNIVA = aNNIVA;
        this.CAUSALE = cAUSALE;
        this.CONTO = cONTO;
        this.IMPORTO = iMPORTO;
        this.SEGNO = sEGNO;
        this.CONTROP = cONTROP;
        this.DESC = dESC;
        this.IVAIND = iVAIND;
        this.TIPOIVA = tIPOIVA;
        this.ALIQIVA = aLIQIVA;
        this.REGIVA = rEGIVA;
        this.IMPONIB = iMPONIB;
        this.STATOCON = sTATOCON;
        this.STATOIVA = sTATOIVA;
        this.TIPOMOV = tIPOMOV;
        this.RIGAGIO = rIGAGIO;
        this.PAGGIO = pAGGIO;
        this.CONCNTRP = cONCNTRP;
    }

    public static Mvcncorr riga1(final XPathEasy in, final String regiva,
            final int anno, final String causale, final String conto,
            final int numreg, final int numrig, final String data,
            final String desc, final Node contropartita) throws Exception {
        final int imponibile = in.getNodeCent(contropartita,
                "IvaNetto/Imponibile");
        final int imposta = in.getNodeCent(contropartita, "IvaNetto/Imposta");
        final int importo = imponibile + imposta;
        return new Mvcncorr(anno, // ANNREG *
                numreg, // NUMREG *
                numrig, // NUMRIG *
                data, // DATREG *
                anno, // ANNIVA
                causale, // CAUSALE
                conto, // CONTO
                ImportExportG1.daCentAValoreConCentesimi(importo), // IMPORTO
                G1.DARE, // SEGNO
                in.getNodeText(contropartita, "Codice"), // CONTROP
                desc, // DESC *
                "", // IVAIND
                "", // TIPOIVA
                in.getNodeText(contropartita, "IvaNetto/Codice"), // ALIQIVA
                regiva, // REGIVA
                "0", // IMPONIB
                "1", // STATOCON
                "0", // STATOIVA
                "4", // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                "1" // CONCNTRP
        );
    }

    public static Mvcncorr riga2(final XPathEasy in, final String regiva,
            final int anno, final String causale, final String controp,
            final int numreg, final int numrig, final String data,
            final String desc, final Node contropartita) throws Exception {
        final int imponibile = in.getNodeCent(contropartita,
                "IvaNetto/Imponibile");
        return new Mvcncorr(anno, // ANNREG *
                numreg, // NUMREG *
                numrig, // NUMRIG *
                data, // DATREG *
                anno, // ANNIVA
                causale, // CAUSALE
                in.getNodeText(contropartita, "Codice"), // CONTO
                ImportExportG1.daCentAValoreConCentesimi(imponibile), // IMPORTO
                G1.AVERE, // SEGNO
                controp, // CONTROP
                desc, // DESC *
                "", // IVAIND
                "", // TIPOIVA
                in.getNodeText(contropartita, "IvaNetto/Codice"), // ALIQIVA
                regiva, // REGIVA
                "0", // IMPONIB
                "1", // STATOCON
                "0", // STATOIVA
                "4", // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                "1" // CONCNTRP
        );
    }

    public static Mvcncorr riga3(final XPathEasy in, final String regiva,
            final int anno, final String causale, final String controp,
            final int numreg, final int numrig, final String data,
            final String desc, final Node contropartita, final String contoIva)
            throws Exception {
        final int imponibile = in.getNodeCent(contropartita,
                "IvaNetto/Imponibile");
        final int imposta = in.getNodeCent(contropartita, "IvaNetto/Imposta");
        return new Mvcncorr(anno, // ANNREG *
                numreg, // NUMREG *
                numrig, // NUMRIG *
                data, // DATREG *
                anno, // ANNIVA
                causale, // CAUSALE
                contoIva, // CONTO
                ImportExportG1.daCentAValoreConCentesimi(imposta), // IMPORTO
                G1.AVERE, // SEGNO
                controp, // CONTROP
                desc, // DESC *
                "", // IVAIND
                "", // TIPOIVA
                in.getNodeText(contropartita, "IvaNetto/Codice"), // ALIQIVA
                regiva, // REGIVA
                ImportExportG1.daCentAValoreConCentesimi(imponibile), // IMPONIB
                "0", // STATOCON
                "1", // STATOIVA
                "4", // TIPOMOV *
                "0", // RIGAGIO
                "0", // PAGGIO
                "1" // CONCNTRP
        );
    }
}
