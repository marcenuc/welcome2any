package it.nuccioservizi.welcome2any.g1;

public class Mvcnprim {
    final int ANNREG;
    final int NUMREG;
    final int NUMRIG;
    final String DATREG;
    final String CAUSALE;
    final String DATDOC;
    final String CONTO;
    final String IMPORTO;
    final String SEGNO;
    final String CONTROP;
    final String DESC;
    final String STATOCON;
    final String TIPOMOV;

    private Mvcnprim(final int aNNREG, final int nUMREG, final int nUMRIG,
            final String dATREG, final String cAUSALE, final String dATDOC,
            final String cONTO, final String iMPORTO, final String sEGNO,
            final String cONTROP, final String dESC, final String sTATOCON,
            final String tIPOMOV) {
        this.ANNREG = aNNREG;
        this.NUMREG = nUMREG * 1000;
        this.NUMRIG = nUMRIG;
        this.DATREG = dATREG;
        this.CAUSALE = cAUSALE;
        this.DATDOC = dATDOC;
        this.CONTO = cONTO;
        this.IMPORTO = iMPORTO;
        this.SEGNO = sEGNO;
        this.CONTROP = cONTROP;
        this.DESC = dESC;
        this.STATOCON = sTATOCON;
        this.TIPOMOV = tIPOMOV;
    }

    public static Mvcnprim pagamento(final String causale, final String data,
            final int anno, final int numreg, final int numrig,
            final String conto, final String desc, final String segno,
            final int importo) {
        return new Mvcnprim( //
                anno, // ANNREG *
                numreg, // NUMREG *
                numrig, // NUMRIG *
                data, // DATREG *
                causale, // CAUSALE *
                data, // DATDOC
                conto, // CONTO
                ImportExportG1.daCentAValoreConCentesimi(importo), // IMPORTO
                segno, // SEGNO
                "", // CONTROP
                desc, // DESC
                "1", // STATOCON
                "0" // TIPOMOV *
        );
    }
}