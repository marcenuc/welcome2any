package it.nuccioservizi.welcome2any.g1;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

import it.nuccioservizi.welcome2any.Props;

public final class Cfg {

    public final Path file_anagrafica_clienti_g1;
    public final String filtro_nomi_file_welcome;
    public final String conto_caparra;
    public final String conto_ricevuta_fiscale;
    public final String causale_fattura;
    public final String causale_nota_di_credito;
    public final String causale_ricevuta_fiscale;
    public final String causale_fattura_di_locazione;
    public final String cod_mastro_clienti;
    public final String conto_di_ricavo_ricevute_generiche;
    public final String conto_di_ricavo_fattura_di_locazione;
    public final String causale_incasso_fattura;
    public final String causale_incasso_nota_di_credito;
    public final String causale_incasso_ricevuta_fiscale;
    public final String causale_incasso_caparra;
    public final String conto_iva_vendite;
    public final String conto_iva_conto_vendite;
    public final String registro_iva_fattura;
    public final String registro_iva_nota_di_credito;
    public final String registro_iva_ricevuta_fiscale;
    public final String registro_iva_fattura_di_locazione;
    public final String suffisso_num_documento_fattura;
    public final String suffisso_num_documento_nota_di_credito;
    public final String suffisso_num_documento_fattura_di_locazione;
    public final String suffisso_num_documento_ricevuta_fiscale;
    public final String causale_welcome_fattura;
    public final String causale_welcome_nota_di_credito;
    public final String causale_welcome_ricevuta_fiscale;

    public Cfg(final Properties props) {
        // @formatter:off
        this.file_anagrafica_clienti_g1 = Props.getPath(props, "file_anagrafica_clienti_g1");
        this.filtro_nomi_file_welcome = Props.getString(props, "filtro_nomi_file_welcome");
        this.conto_caparra = Props.getString(props, "conto_caparra");
        this.conto_ricevuta_fiscale = Props.getString(props, "conto_ricevuta_fiscale");
        this.causale_fattura = Props.getString(props, "causale_fattura");
        this.causale_nota_di_credito = Props.getString(props, "causale_nota_di_credito");
        this.causale_ricevuta_fiscale = Props.getString(props, "causale_ricevuta_fiscale");
        this.causale_fattura_di_locazione = Props.getString(props, "causale_fattura_di_locazione");
        this.cod_mastro_clienti = Props.getString(props, "cod_mastro_clienti");
        this.conto_di_ricavo_ricevute_generiche = Props.getString(props, "conto_di_ricavo_ricevute_generiche");
        this.conto_di_ricavo_fattura_di_locazione = Props.getString(props, "conto_di_ricavo_fattura_di_locazione");
        this.causale_incasso_fattura = Props.getString(props, "causale_incasso_fattura");
        this.causale_incasso_nota_di_credito = Props.getString(props, "causale_incasso_nota_di_credito");
        this.causale_incasso_ricevuta_fiscale = Props.getString(props, "causale_incasso_ricevuta_fiscale");
        this.causale_incasso_caparra = Props.getString(props, "causale_incasso_caparra");
        this.conto_iva_vendite = Props.getString(props, "conto_iva_vendite");
        this.conto_iva_conto_vendite = Props.getString(props, "conto_iva_conto_vendite");
        this.registro_iva_fattura = Props.getString(props, "registro_iva_fattura");
        this.registro_iva_nota_di_credito = Props.getString(props, "registro_iva_nota_di_credito");
        this.registro_iva_ricevuta_fiscale = Props.getString(props, "registro_iva_ricevuta_fiscale");
        this.registro_iva_fattura_di_locazione = Props.getString(props, "registro_iva_fattura_di_locazione");
        this.suffisso_num_documento_fattura = Props.getString(props, "suffisso_num_documento_fattura");
        this.suffisso_num_documento_nota_di_credito = Props.getString(props, "suffisso_num_documento_nota_di_credito");
        this.suffisso_num_documento_fattura_di_locazione = Props.getString(props, "suffisso_num_documento_fattura_di_locazione");
        this.suffisso_num_documento_ricevuta_fiscale = Props.getString(props, "suffisso_num_documento_ricevuta_fiscale");
        this.causale_welcome_fattura = Props.getString(props, "causale_welcome_fattura");
        this.causale_welcome_nota_di_credito = Props.getString(props, "causale_welcome_nota_di_credito");
        this.causale_welcome_ricevuta_fiscale = Props.getString(props, "causale_welcome_ricevuta_fiscale");
        // @formatter:on
    }

    public static Cfg load(final Path propsPath) throws IOException {
        return new Cfg(Props.load(propsPath));
    }
}
