package it.nuccioservizi.welcome2any.g1;

import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;

import org.w3c.dom.Node;

import it.nuccioservizi.welcome2any.U;
import it.nuccioservizi.welcome2any.UI;
import it.nuccioservizi.welcome2any.Welcome;
import it.nuccioservizi.welcome2any.XPathEasy;

/**
 * @author marcenuc
 *
 *         Conversione da installazione Welcome di Callistos.
 */
public final class Callistos {
    private static final String CONFIG_FILE_NAME = "callistos.properties";

    private final Path cartellaDiLavoro;

    public Callistos(final Path cartellaDiLavoro) {
        this.cartellaDiLavoro = cartellaDiLavoro;
    }

    public static void main(final String[] args) throws Exception {
        try {
            UI.init();

            final Callistos app = new Callistos(U.getUserDir());

            final Cfg cfg = Cfg
                    .load(app.getPathInCartellaDiLavoro(CONFIG_FILE_NAME));
            final List<Path> fileLetti = new ArrayList<>();
            final List<String> clientiFatture = new ArrayList<>();
            final ImportExportG1 ieg1 = ImportExportG1.of(cfg);

            final DocumentBuilder documentBuilder = U
                    .newNonValidatingDocumentBuilder();

            final XPath xPath = XPathEasy.newXPath();

            try (final DirectoryStream<Path> welcomePathsStream = U
                    .newDirectoryStream(app.cartellaDiLavoro,
                            cfg.filtro_nomi_file_welcome)) {

                for (final Path welcomePath : welcomePathsStream) {
                    if (!UI.sìNo("Leggo il file " + welcomePath + "?"))
                        continue;

                    final XPathEasy in = XPathEasy.fromFile(xPath,
                            documentBuilder, welcomePath);
                    fileLetti.add(welcomePath);

                    for (final Node fattura : in
                            .getNodes(Welcome.XPATH_FATTURA)) {
                        final String causale = in.getNodeText(fattura,
                                Welcome.XPATH_CAUSALE);
                        if (cfg.causale_welcome_fattura.equals(causale)
                                || cfg.causale_welcome_nota_di_credito
                                        .equals(causale))
                            clientiFatture.add(in.getNodeText(fattura,
                                    Welcome.XPATH_CODICE_CLIENTE));
                    }

                    for (final Node cliente : in
                            .getNodes(Welcome.XPATH_CLIENTE))
                        if (clientiFatture.contains(
                                in.getNodeText(cliente, Welcome.XPATH_CODICE)))
                            ieg1.addCliente(cliente);

                    for (final Node giorno : in
                            .getNodes(Welcome.XPATH_GIORNO)) {
                        final LocalDate data = Welcome.parseLocalDate(
                                in.getNodeText(giorno, Welcome.XPATH_DATA));

                        for (final Node fattura : in.getNodes(giorno,
                                Welcome.XPATH_FATTURA_GIORNO)) {
                            final String causale = in.getNodeText(fattura,
                                    Welcome.XPATH_CAUSALE);

                            if (cfg.causale_welcome_ricevuta_fiscale
                                    .equals(causale))
                                ieg1.addRicevutaFiscale(in, data, fattura);
                            else if (cfg.causale_welcome_fattura
                                    .equals(causale))
                                ieg1.addFatturaONotaDiCredito(in, data, fattura,
                                        false);
                            else if (cfg.causale_welcome_nota_di_credito
                                    .equals(causale))
                                ieg1.addFatturaONotaDiCredito(in, data, fattura,
                                        true);
                            else
                                throw new IllegalStateException(
                                        "Causale fattura sconosciuta: "
                                                + causale);
                        }

                        for (final Node anticipo : in.getNodes(giorno,
                                Welcome.XPATH_ANTICIPO_GIORNO))
                            ieg1.addInserimentoCaparra(in, data, anticipo);
                    }
                }
            }

            final Path filePerG1 = app
                    .getPathInCartellaDiLavoro("importG1.xml");

            try (final OutputStream xmlG1 = U.newOutputStream(filePerG1)) {
                ieg1.scriviXml(xmlG1);
            }

            {
                final StringBuilder report = new StringBuilder("<html>");
                report.append("<h1>Conversione completata</h1>") //
                        .append("<dl><dt>Cartella di lavoro:</dt><dd><p>") //
                        .append(app.cartellaDiLavoro) //
                        .append("</p></dd><dt>File letti:</dt><dd><ol>");
                for (final Path f : fileLetti)
                    report //
                            .append("<li>") //
                            .append(f.getFileName()) //
                            .append("</li>");
                report.append("</ol></dd><dt>File generato:</dt><dd><p>")
                        .append(filePerG1.getFileName()) //
                        .append("</p></dd></dl>") //
                        .append("</html>");

                UI.info(report);
            }
        } catch (final Exception e) {
            UI.error(e);
            throw e;
        }
    }

    private Path getPathInCartellaDiLavoro(final String fileName) {
        return U.getPath(this.cartellaDiLavoro, fileName);
    }

}
