package it.nuccioservizi.welcome2any.movago;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.eclipse.jdt.annotation.NonNull;

import it.nuccioservizi.welcome2any.U;

public final class FileMovago implements AutoCloseable {
	final int progressivo;
	private final BufferedWriter out;
	private final Path outPath;
	final TipoRecord trI;
	final TipoRecord trD10;
	final TipoRecord trD11;
	final TipoRecord trD30;
	final TipoRecord trD31;
	final TipoRecord trD40;
	final TipoRecord trD50;
	final TipoRecord trC;

	private FileMovago(final int progressivo, final BufferedWriter out, final Path outPath, final TipoRecord trI,
			final TipoRecord trD10, final TipoRecord trD11, final TipoRecord trD30, final TipoRecord trD31,
			final TipoRecord trD40, final TipoRecord trD50, final TipoRecord trC) {
		this.progressivo = progressivo;
		this.out = out;
		this.outPath = outPath;
		this.trI = trI;
		this.trD10 = trD10;
		this.trD11 = trD11;
		this.trD30 = trD30;
		this.trD31 = trD31;
		this.trD40 = trD40;
		this.trD50 = trD50;
		this.trC = trC;
	}

	static FileMovago fromCounter(final Contatore c, final Path csvTipiRecord) throws Exception {
		final int progressivo = c.succ();
		final Path outPath = getPath(progressivo);

		final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
				Files.newOutputStream(outPath, StandardOpenOption.CREATE_NEW), StandardCharsets.ISO_8859_1));
		final MetaMovago meta = MetaMovago.fromCsv(csvTipiRecord);

		return new FileMovago(
				//
				progressivo, //
				out, //
				outPath, //
				meta.tipiRecord.get("I"), //
				meta.tipiRecord.get("D10"), //
				meta.tipiRecord.get("D11"), //
				meta.tipiRecord.get("D30"), //
				meta.tipiRecord.get("D31"), //
				meta.tipiRecord.get("D40"), //
				meta.tipiRecord.get("D50"), //
				meta.tipiRecord.get("C"));
	}

	private static Path getPath(final int progressivo) {
		@SuppressWarnings("null")
		@NonNull
		final String name = String.format("MOVAGO-%d.txt", Integer.valueOf(progressivo));

		return U.getPath(name);
	}

	Path getOutPath() {
		return outPath;
	}

	public void write(final String str) throws IOException {
		out.write(str);
	}

	@Override
	public void close() throws Exception {
		out.close();
	}

}
