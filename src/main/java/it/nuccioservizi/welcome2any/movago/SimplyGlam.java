package it.nuccioservizi.welcome2any.movago;

import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Node;

import it.nuccioservizi.welcome2any.U;
import it.nuccioservizi.welcome2any.UI;
import it.nuccioservizi.welcome2any.Welcome;
import it.nuccioservizi.welcome2any.Welcome.TipoFattura;
import it.nuccioservizi.welcome2any.XPathEasy;
import it.nuccioservizi.welcome2any.XmlUtil;

/**
 * @author marcenuc
 *
 *         Conversione da installazione Welcome di SimplyGlam.
 */
public final class SimplyGlam {
	private static final String CONFIG_FILE_NAME = "simply-glam.properties";

	private final Path cartellaDiLavoro;
	private final Cfg cfg;
	private final Welcome welcome;

	public SimplyGlam(final Path cartellaDiLavoro, final Cfg cfg, final Welcome welcome) {
		this.cartellaDiLavoro = cartellaDiLavoro;
		this.cfg = cfg;
		this.welcome = welcome;
	}

	public static void main(final String[] args) throws Exception {
		try {
			UI.init();
			final Path cartellaDiLavoro = U.getUserDir();
			final Cfg cfg = Cfg.load(U.getPath(cartellaDiLavoro, CONFIG_FILE_NAME));

			final SimplyGlam app = new SimplyGlam(cartellaDiLavoro, cfg,
					new Welcome(new TipoFattura(cfg.suffisso_num_documento_fattura, //
							cfg.causale_contabile_fattura_vendita, //
							1, //
							1),
							new TipoFattura(cfg.suffisso_num_documento_nota_di_credito, //
									cfg.causale_contabile_nota_di_credito_vendita, //
									1, //
									1)));

			final List<Path> fileLetti = new ArrayList<>();
			final Set<String> clientiFatture = new HashSet<>();
			final List<Pair<LocalDate, Node>> ricevuteFiscali = new ArrayList<>();
			final List<Pair<LocalDate, Node>> fatture = new ArrayList<>();
			final List<Pair<LocalDate, Node>> noteDiCredito = new ArrayList<>();
			final List<Pair<LocalDate, Node>> anticipi = new ArrayList<>();

			final DocumentBuilder documentBuilder = U.newNonValidatingDocumentBuilder();

			final XPath xPath = XPathEasy.newXPath();

			try (final DirectoryStream<Path> welcomePathsStream = U.newDirectoryStream(app.cartellaDiLavoro,
					cfg.filtro_nomi_file_welcome)) {

				for (final Path welcomePath : welcomePathsStream) {
					if (!UI.sìNo("Leggo il file " + welcomePath + "?"))
						continue;

					final XPathEasy in = XPathEasy.fromFile(xPath, documentBuilder, welcomePath);
					fileLetti.add(welcomePath);

					for (final Node giorno : in.getNodes(Welcome.XPATH_GIORNO)) {
						final LocalDate data = Welcome.parseLocalDate(in.getNodeText(giorno, Welcome.XPATH_DATA));

						for (final Node fattura : in.getNodes(giorno, Welcome.XPATH_FATTURA_GIORNO)) {
							final String causale = in.getNodeText(fattura, Welcome.XPATH_CAUSALE);

							if (cfg.causale_welcome_ricevuta_fiscale.equals(causale))
								ricevuteFiscali.add(U.pairOf(data, fattura));
							else {
								final String codCli = in.getNodeText(fattura, Welcome.XPATH_CODICE_CLIENTE);

								if (cfg.causale_welcome_fattura.equals(causale)) {
									fatture.add(U.pairOf(data, fattura));
									clientiFatture.add(codCli);
								} else if (cfg.causale_welcome_nota_di_credito.equals(causale)) {
									noteDiCredito.add(U.pairOf(data, fattura));
									clientiFatture.add(codCli);
								} else
									throw new IllegalStateException("Causale fattura sconosciuta: " + causale);
							}
						}

						for (final Node anticipo : in.getNodes(giorno, Welcome.XPATH_ANTICIPO_GIORNO))
							anticipi.add(U.pairOf(data, anticipo));
					}

					final LocalDate dataImportazione = U.now();
					@NonNull
					final Path outPath;

					try (final Contatore progressivoImportazioni = Contatore
							.fromFile(U.getPath("progressivo_importazioni.txt"));
							final Contatore progressivo = Contatore.fromFile(U.getPath("progressivo_record.txt"));
							final FileMovago out = FileMovago.fromCounter(progressivoImportazioni,
									U.getPath("MOVAGO-records500.csv"))) {
						outPath = out.getOutPath();

						out.trI.setI(cfg.cod_ditta, out.progressivo, dataImportazione, cfg.cod_applicazione);
						out.write(out.trI.print());

						for (final Node cliente : in.getNodes(Welcome.XPATH_CLIENTE))
							if (clientiFatture.contains(in.getNodeText(cliente, Welcome.XPATH_CODICE)))
								clienteToRecordD10D11(out, cliente);

						for (final Pair<LocalDate, Node> dataEDoc : fatture)
							app.fatturaONotaDiCreditoToRecordD30D31(out, progressivo.succ(), in, dataEDoc, false);

						for (final Pair<LocalDate, Node> dataEDoc : noteDiCredito)
							app.fatturaONotaDiCreditoToRecordD30D31(out, progressivo.succ(), in, dataEDoc, true);

						for (final Pair<LocalDate, Node> dataEDoc : ricevuteFiscali)
							app.corrispettivoToRecordD40(out, progressivo, in, dataEDoc);

						for (final Pair<LocalDate, Node> dataEDoc : fatture)
							app.fatturaToRecordD50(out, progressivo, in, dataEDoc, false);

						for (final Pair<LocalDate, Node> dataEDoc : noteDiCredito)
							app.fatturaToRecordD50(out, progressivo, in, dataEDoc, true);

						for (final Pair<LocalDate, Node> dataEDoc : ricevuteFiscali)
							app.pagamentiECaparreToRecordD50(out, progressivo, in, dataEDoc);

						for (final Pair<LocalDate, Node> dataEDoc : anticipi)
							anticipoToRecordD50(out, progressivo, in, dataEDoc);

						out.trC.setC(cfg.cod_ditta, out.progressivo, dataImportazione, cfg.cod_applicazione);
						out.write(out.trC.print());
					}
					{
						final StringBuilder report = new StringBuilder("<html>");
						report.append("<h1>Conversione completata</h1>") //
								.append("<dl><dt>Cartella di lavoro:</dt><dd><p>") //
								.append(app.cartellaDiLavoro) //
								.append("</p></dd><dt>File letti:</dt><dd><ol>");
						for (final Path f : fileLetti)
							report.append("<li>").append(f.getFileName()).append("</li>");
						report.append("</ol></dd><dt>File generato:</dt><dd><p>").append(outPath)
								.append("</p></dd></dl>") //
								.append("</html>");

						UI.info(report);
					}
				}
			}

		} catch (final Exception e) {
			UI.error(e);
			throw e;
		}
	}

	private static void clienteToRecordD10D11(final FileMovago out, final Node cliente) throws Exception {
		final Map<String, String> vals = XmlUtil.nodeToMap(cliente);
		@Nullable
		final String denominazione = vals.get("RagioneSociale");
		@Nullable
		final String cognome = vals.get("Cognome");
		@Nullable
		final String nome = vals.get("Nome");
		@Nullable
		final String codiceFiscale = vals.get("CodiceFiscale");
		@Nullable
		final String codiceDaGestionale = vals.get("Codice");

		@Nullable
		final String indirizzo = vals.get("Indirizzo");
		@Nullable
		final String capComune = vals.get("Cap");
		final String partitaIva = U.filtraCF(vals.get("PartitaIVA"));
		final Optional<LocalDate> dataDiNascita = Welcome.parseOptionalLocalDate(vals.get("DataNascita"));
		@Nullable
		final String codIsoEstero = vals.get("SiglaNazione");

		final String numeroCivico = "";
		final String codComune = "";
		final String sesso = "";

		final boolean isItaliano = "IT".equals(codIsoEstero);
		final boolean isSocietà = "N".equals(vals.get("PersonaFisica"));

		out.trD10.setD10(denominazione, cognome, nome, codiceFiscale, isItaliano, isSocietà, codiceDaGestionale);
		out.write(out.trD10.print());
		out.trD11.setD11(isItaliano, indirizzo, numeroCivico, codComune, capComune, partitaIva, codiceFiscale,
				dataDiNascita, codIsoEstero, sesso);
		out.write(out.trD11.print());
	}

	private void fatturaONotaDiCreditoToRecordD30D31(final FileMovago out, final int progressivo, final XPathEasy in,
			final Pair<LocalDate, Node> dataEDoc, final boolean isNotaDiCredito) throws Exception {
		final LocalDate dataDocumento = U.getLeft(dataEDoc);
		final Node docFattura = U.getRight(dataEDoc);
		final List<Node> contropartite = in.getNodes(docFattura, "Contropartite/Contropartita");
		final TipoFattura tipoFattura = welcome.toTipoFattura(isNotaDiCredito);
		final int numdocW = in.getNodeInt(docFattura, "Numero");
		final String numdoc = numdocW + "/" + tipoFattura.suffisso;
		final String codClienteGestionale = in.getNodeText(docFattura, "CodiceCliente");
		final int totDocumento = in.getNodeCent(docFattura, "TotaleDocumento");

		out.trD30.setD30(tipoFattura.causale, tipoFattura.attivitàIva, tipoFattura.sezionaleIva, numdocW, numdoc,
				U.optionalOf(dataDocumento), codClienteGestionale, totDocumento, progressivo);
		out.write(out.trD30.print());

		for (final Node contropartita : contropartite) {
			final int imponibile = in.getNodeCent(contropartita, "IvaNetto/Imponibile");
			final int imposta = in.getNodeCent(contropartita, "IvaNetto/Imposta");
			final String codIvaENorma = in.getNodeText(contropartita, "IvaNetto/Codice");
			final String codIva = U.estraiCodIva(codIvaENorma);
			final String codNorma = U.estraiCodNormaIva(codIvaENorma);
			final String codContropartita = in.getNodeText(contropartita, "Codice");

			out.trD31.setD31(codIva, imponibile, imposta, codNorma, codContropartita);
			out.write(out.trD31.print());
		}
	}

	private void corrispettivoToRecordD40(final FileMovago out, final Contatore progressivo, final XPathEasy in,
			final Pair<LocalDate, Node> dataEDoc) throws Exception {
		final Optional<LocalDate> data = U.optionalOf(U.getLeft(dataEDoc));
		final Node docFattura = U.getRight(dataEDoc);
		final List<Node> contropartite = in.getNodes(docFattura, "Contropartite/Contropartita");
		for (final Node contropartita : contropartite) {
			final int imponibile = in.getNodeCent(contropartita, "IvaNetto/Imponibile");
			final int imposta = in.getNodeCent(contropartita, "IvaNetto/Imposta");
			final int importo = imponibile + imposta;
			final String codIvaENorma = in.getNodeText(contropartita, "IvaNetto/Codice");
			final String codIva = U.estraiCodIva(codIvaENorma);
			final String codNorma = U.estraiCodNormaIva(codIvaENorma);
			final String codContropartita = in.getNodeText(contropartita, "Codice");
			final int rfDalNum = in.getNodeInt(docFattura, "Numero");
			final int rfAlNum = rfDalNum;

			out.trD40.setD40(data, codIva, importo, rfDalNum, rfAlNum, codNorma, codContropartita,
					cfg.conto_clienti_corrispettivi_da_incassare, progressivo.succ());
			out.write(out.trD40.print());
		}

	}

	private void fatturaToRecordD50(final FileMovago out, final Contatore progressivo, final XPathEasy in,
			final Pair<LocalDate, Node> dataEDoc, final boolean isNotaDiCredito) throws Exception {
		final LocalDate dataDocumento = U.getLeft(dataEDoc);
		final Node docFattura = U.getRight(dataEDoc);
		final TipoFattura tipoFattura = welcome.toTipoFattura(isNotaDiCredito);
		final int numdocW = in.getNodeInt(docFattura, "Numero");
		final String numdoc = numdocW + "/" + tipoFattura.suffisso;
		final String codClienteGestionale = in.getNodeText(docFattura, "CodiceCliente");
		final Optional<LocalDate> data = U.optionalOf(dataDocumento);
		final String descrOperazione = numdoc + " " + dataDocumento;
		final String codContoAvere = codClienteGestionale;
		final String clienteOFornitore = "001";
		final String codContoClienti = cfg.conto_mastro_clienti;
		final List<Node> pagamenti = in.getNodes(docFattura, "Pagamento");

		for (final Node pagamento : pagamenti) {
			final int importo = in.getNodeCent(pagamento, "Importo");
			final String codSottocontoDare = in.getNodeText(pagamento, "Codice");
			final int p = progressivo.succ();

			out.trD50.setD50(data, descrOperazione, importo, "", codContoAvere, "", "", codContoClienti,
					clienteOFornitore, p);
			out.write(out.trD50.print());
			out.trD50.setD50(data, descrOperazione, importo, "", "", codSottocontoDare, "", "", "",
					p);
			out.write(out.trD50.print());
		}
	}

	private void pagamentiECaparreToRecordD50(final FileMovago out, final Contatore progressivo, final XPathEasy in,
			final Pair<LocalDate, Node> dataEDoc) throws Exception {
		final LocalDate dataDocumento = U.getLeft(dataEDoc);
		final Optional<LocalDate> data = U.optionalOf(dataDocumento);
		final Node docFattura = U.getRight(dataEDoc);
		final int numdocW = in.getNodeInt(docFattura, "Numero");
		final String numdoc = numdocW + "/" + cfg.suffisso_num_documento_ricevuta_fiscale;
		final String descrizione = numdoc + " " + dataDocumento;
		final List<Node> pagamenti = in.getNodes(docFattura, "Pagamento");

		for (final Node pagamento : pagamenti) {
			final int importo = in.getNodeCent(pagamento, "Importo");
			final String codDare = in.getNodeText(pagamento, "Codice");
			final String codAvere = cfg.conto_clienti_corrispettivi_da_incassare;
			final int p = progressivo.succ();

			out.trD50.setD50(data, descrizione, importo, "", "", codDare, "", "", "", p);
			out.write(out.trD50.print());
			out.trD50.setD50(data, descrizione, importo, "", "", "", codAvere, "", "", p);
			out.write(out.trD50.print());
		}
	}

	private static void anticipoToRecordD50(final FileMovago out, final Contatore progressivo, final XPathEasy in,
			final Pair<LocalDate, Node> dataEDoc) throws Exception {
		final LocalDate dataDocumento = U.getLeft(dataEDoc);
		final Optional<LocalDate> data = U.optionalOf(dataDocumento);
		final Node doc = U.getRight(dataEDoc);
		final String descrizione = in.getNodeText(doc, "Descrizione");
		final int importo = in.getNodeCent(doc, "Importo");
		final String codSottocontoDare = in.getNodeText(doc, "ContoPagamento");
		final String codSottocontoAvere = in.getNodeText(doc, "ContoAnticipo");

		final int p = progressivo.succ();

		out.trD50.setD50(data, descrizione, importo, "", "", codSottocontoDare, "", "", "", p);
		out.write(out.trD50.print());
		out.trD50.setD50(data, descrizione, importo, "", "", "", codSottocontoAvere, "", "", p);
		out.write(out.trD50.print());
	}
}
