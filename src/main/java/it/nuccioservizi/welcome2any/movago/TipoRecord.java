package it.nuccioservizi.welcome2any.movago;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Optional;

import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import it.nuccioservizi.welcome2any.U;

final class TipoRecord {
    /** Identificativo per il tipo record. */
    final String codTipo;
    final NavigableSet<Campo> campi;
    final Map<String, Campo> nomiCampi;

    TipoRecord(final String codTipo, final NavigableSet<Campo> campi) {
        this.codTipo = codTipo;
        this.campi = campi;

        final Map<String, Campo> m = new HashMap<>();
        for (final Campo c : campi)
            m.put(c.nome, c);
        nomiCampi = U.asUnmodifiableMap(m);
    }

    boolean dataIsRecordOfThisType(final String data) {
        return data.startsWith(codTipo);
    }

    public void print(final String record) {
        for (final Campo campo : campi)
            System.out
                    .println(
                            String.format(
                                    "%s=%s", campo.nome,
                                    record.substring(campo.inizio - 1, campo.fine)
                            )
                    );
        System.out.println();
    }

    @Override
    public String toString() {
        return "TipoRecord [codTipo=" + codTipo + ", campi=" + campi + "]";
    }

    /*
     * --------------- GESTIONE VALORI ---------------
     */

    private final Map<String, String> valori = new HashMap<>();

    public TipoRecord resetValori() {
        valori.clear();

        return this;
    }

    public TipoRecord set(final String nomeCampo, final int valore) {
        final String s = Integer.toString(valore);

        return set(nomeCampo, Validate.notNull(s));
    }

    public TipoRecord set(final String nomeCampo, final Optional<LocalDate> date) {
        return set(nomeCampo, TipoCampo.D.print(date));
    }

    public TipoRecord set(final String nomeCampo, final LocalDate date) {
        final Optional<LocalDate> o = Optional.of(date);
        Validate.notNull(o);

        return set(nomeCampo, TipoCampo.D.print(o));
    }

    public TipoRecord set(final String nomeCampo, final String valore) {
        Validate.isTrue(
                nomiCampi.containsKey(nomeCampo), "Il campo %s è sconosciuto. Nomi validi: %s", nomeCampo, nomiCampi.keySet()
        );

        @SuppressWarnings("null")
        @NonNull
        final Campo campo = nomiCampi.get(nomeCampo);

        if (valore.length() > campo.lunghezza)
            System.err.println(
                    String.format(
                            "ATTENZIONE: il valore \"%s\" verrà troncato perché troppo lungo per il campo %s",
                            valore, campo
                    )
            );

        valori.put(nomeCampo, valore);

        return this;
    }

    private @Nullable Campo prossimoCampo(final Campo campo) {
    	return campi.higher(campo);
    }
    
    public String print() {
        final StringBuilder str = new StringBuilder();
        int prossimoIdxInizio = 1;

        for (final Campo campo : campi)
            // Più campi possono avere lo stesso inizio.
            // Noi scriviamo solo il primo che ha un valore impostato,
            // poi scartiamo tutti i successivi "coperti" dal precedente.
            if (prossimoIdxInizio == campo.inizio) {
                if (!valori.containsKey(campo.nome)) {
                    final Campo prossimoCampo = prossimoCampo(campo);
                    if (prossimoCampo != null && prossimoCampo.inizio == campo.inizio)
                        // Non abbiamo un valore e il campo successivo ha lo stesso inizio:
                        // non scriviamo niente, magari c'è un valore per il prossimo campo.
                        continue;
                }
                str.append(campo.tipo.print(valori.getOrDefault(campo.nome, ""), campo.lunghezza));

                prossimoIdxInizio += campo.lunghezza;
            }

        @SuppressWarnings("null")
        @NonNull
        final String s = str.toString();
        return s;
    }

    /**
     * TODO: questo controllo dovrebbe essere fatto a tempo di compilazione.
     *
     * @param codTipoAtteso
     */
    private void validateCodTipo(final String codTipoAtteso) {
        Validate.isTrue(
                codTipo.equals(codTipoAtteso), "Questo è un record di tipo %s, non %s", codTipo, codTipoAtteso
        );
    }

    public void setI(final int codDitta, final int numImportazione,
            final LocalDate dataImportazione, final String codApplicazione) {
        validateCodTipo("I");
        resetValori();
        set("CodTipoRec", "I");
        set("CFCSOGG", codDitta);
        set("CFNPROGVSCH", 1);
        set("NumSped", numImportazione);
        set("DataSped", dataImportazione);
        set("AppReg", codApplicazione);
    }

    public void setC(final int codDitta, final int numImportazione,
            final LocalDate dataImportazione, final String codApplicazione) {
        validateCodTipo("C");
        resetValori();
        set("CodTipoRec", "C");
        set("CFCSOGG", codDitta);
        set("CFNPROGVSCH", 1);
        set("NumSped", numImportazione);
        set("DataSped", dataImportazione);
        set("AppReg", codApplicazione);
    }

    public void setD10(final String denominazione, final String cognome, final String nome,
            final String codiceFiscale, final boolean isItaliano, final boolean isSocietà,
            final String codiceGestionale) {
        validateCodTipo("D10");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 10);
        set("CFCTIPOCLINFORN", "1");

        if (isSocietà)
            set("SGXDENMN", denominazione);
        else
            set("SGXCOGN", cognome) //
                    .set("SGXNOME", nome);

        if (isItaliano)
            set("SGCFISC", codiceFiscale);

        set("SGCNATSOG", isItaliano ? "001" : "003");
        set("SGCTIPOANAG", isSocietà ? "SOC" : "PER");
        set("Codice gestionale", codiceGestionale);
    }

    public void setD11(final boolean isItaliano, final String indirizzo, final String numeroCivico,
            final String codComune, final String capComune, final String partitaIva,
            final String codiceFiscale, final Optional<LocalDate> dataDiNascita,
            final String codIsoEstero, final String sesso) {
        validateCodTipo("D11");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 11);

        if (!isItaliano)
            set("SGCIDENVFISCEST", codiceFiscale);

        set("INXINDR", indirizzo);
        set("INNCIV", numeroCivico);
        set("INCCOM", codComune);
        set("INCCAP", capComune);
        set(isItaliano ? "SGCPARTIVA" : "SGCNUMIDENVIVAEST", partitaIva);
        set("SGSNASC", dataDiNascita);
        set("SGCCODISOEST", codIsoEstero);
        set("SGCSES", sesso);
    }

    public void setD30(final String codCausale, final int codAttivitàIva, final int codSezionaleIva,
            final int numProtocollo, final String numDoc, final Optional<LocalDate> giorno,
            final String codClienteGestionale, final int totDocumento, final int progressivo) {
        validateCodTipo("D30");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 30);
        set("FACCAUS", codCausale);
        set("FACTIPOFATT", 1);
        set("FANATTVIVA", codAttivitàIva);
        set("FACSEZNIVA", codSezionaleIva);
        set("FANPROT", numProtocollo);
        set("FANDOCM", numDoc);
        set("FADREGSN", giorno);
        set("FADDOCM", giorno);
        set("FACSOGGCLINFORN", codClienteGestionale);
        set("FAFOPPSNASSS", 1);
        set("FAITOTDOCM", totDocumento);
        set("FANREGSN", progressivo);
    }

    public void setD31(final String codIva, final int imponibile, final int imposta,
            final String codNorma, final String codContropartita) {
        validateCodTipo("D31");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 31);
        set("FACIVA", codIva);
        set("FAIIMPN", imponibile);
        set("FAIMP", imposta);
        set("FACNOR", codNorma);
        set("CSotConCosRic_suggest", codContropartita);
    }

    public void setD40(final Optional<LocalDate> giorno, final String codIva, final int importo,
            final int rfDalNum, final int rfAlNum, final String codNorma, final String codConto,
            final String codContoDare, final int progressivo) {
        validateCodTipo("D40");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 40);
        set("CONATTVIVA", 1);
        set("COCSEZNIVA", 1);
        set("COCCAUS", 1005);
        set("CODREGSN", giorno);
        set("COCIVA", codIva);
        set("SEGIMP", importo < 0 ? "-" : "");
        set("COIOPER", importo < 0 ? -importo : importo);
        set("CONCORRVDAL", rfDalNum);
        set("CONCORRVAL", rfAlNum);
        set("COCNOR", codNorma);
        set("CSotConRic_suggest", codConto);
        set("CSotConDare_suggest", codContoDare);
        set("CONREGSN", progressivo);
    }

    public void setD50(final Optional<LocalDate> giorno, final String descrOperazione,
            final int importo, final String codContoDare, final String codContoAvere,
            final String codSottocontoDare, final String codSottocontoAvere,
            final String codContoCliente, final String clienteOFornitore, final int progressivo) {
        validateCodTipo("D50");
        resetValori();
        set("CodTipoRec", "D");
        set("CodTipoDett", 50);
        set("PNDREGSN", giorno);
        set("PNCCAUS", "2001");
        set("PNXOPER", descrOperazione.isEmpty() ? "INCASSO" : descrOperazione);
        set("PNIOPER", importo);
        set("CSotConDare_suggest", codSottocontoDare);
        set("CSotConAve_suggest", codSottocontoAvere);
        set("PNCTIPOCLINFORN", clienteOFornitore);
        set("PNCSOGGCLINFORNDARE", codContoDare);
        set("PNCSOGGCLINFORNAVE", codContoAvere);
        set("CSotConAbbn_suggest", codContoCliente);
        set("PNNREGSN", progressivo);
    }
}
