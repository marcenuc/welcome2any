package it.nuccioservizi.welcome2any.movago;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jdt.annotation.NonNull;

import it.nuccioservizi.welcome2any.U;

final class MetaMovago {
    final Map<String, TipoRecord> tipiRecord;
    private final int recordSize;

    MetaMovago(final Map<String, TipoRecord> tipiRecord, final int recordSize) {
        this.tipiRecord = tipiRecord;
        this.recordSize = recordSize;
    }

    private static void addTipoRecord(final Map<String, TipoRecord> tipiRecord,
            final String codTipoRecord, final NavigableSet<Campo> campi, final int recordSize) {
        if (codTipoRecord.isEmpty() && !campi.isEmpty())
            throw new IllegalStateException("Campi senza codice tipo record: " + campi);

        final int thisSize = sizeTipoRecord(campi);
        if (recordSize != thisSize)
            throw new IllegalStateException(
                    String.format(
                            "Il tipo record %s ha dimensione %s, ma dovrebbe essere di %s caratteri.",
                            codTipoRecord, String.valueOf(thisSize), String.valueOf(recordSize)
                    )
            );

        tipiRecord.put(codTipoRecord, new TipoRecord(codTipoRecord, campi));
    }

    @SuppressWarnings("null")
    private static int sizeTipoRecord(final NavigableSet<Campo> campi) {
        return campi.last().fine;
    }

    public static MetaMovago fromCsv(final Path csvTipiRecord) throws IOException {
        /*
         * Ogni tipo record ha una testata che serve per:
         *
         * - ottenere il codice che identifica il record, da cercare nei primi caratteri
         * del record letto.
         *
         * - sapere dove comincia la tabella che descrive i campi del record (nel record
         * successivo).
         */
        final Pattern tipoRecordIdPattern = Pattern
                .compile("\\AID-Formato: CodTipoRec=.(.).(?: CodTipoDett=(\\S+))?.*");

        final Map<String, TipoRecord> tipiRecord = new HashMap<>();
        // Modificato solo una volta al caricamento del primo tipo record letto
        int recordSize = 0;

        try (
                final InputStream is = MetaMovago.class.getResourceAsStream(csvTipiRecord.toString()); //
                final Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8); //
                final CSVParser parser = CSVFormat.DEFAULT.parse(reader)
        ) {

            boolean nextRecordIsHeaderCampi = false;
            String codTipoRecord = "";
            NavigableSet<Campo> campi = new TreeSet<>();

            for (final CSVRecord record : parser) {
                if (nextRecordIsHeaderCampi) {
                    nextRecordIsHeaderCampi = false;
                    // Le intestazioni non ci interessano
                    continue;
                }

                final String nome = record.get(0);
                if (nome == null)
                    throw new IllegalStateException("Manca il primo campo");

                if (nome.isEmpty())
                    continue;

                final Matcher idMatcher = tipoRecordIdPattern.matcher(nome);
                if (idMatcher.matches()) {
                    nextRecordIsHeaderCampi = true;

                    if (!campi.isEmpty()) {
                        // Abbiamo letto una tabella campi: la salviamo

                        if (recordSize == 0)
                            // È il primo tipo record: ricordiamo la dimensione
                            recordSize = sizeTipoRecord(campi);

                        addTipoRecord(tipiRecord, codTipoRecord, campi, recordSize);

                        campi = new TreeSet<>();
                    }

                    codTipoRecord = idMatcher.group(1)
                            + Optional.ofNullable(idMatcher.group(2)).orElse("");
                } else {
                    final String tipoInput = U.toUpperCase(U.getString(record, 1));
                    final String tipo = tipoInput.isEmpty() ? "FILLER"
                            : tipoInput.equals("DT") ? "D" : tipoInput;
                    final int inizio = U.getInt(record, 2);
                    final int fine = U.getInt(record, 3);
                    final String note = U.getStringIfExists(record, 5);

                    campi.add(new Campo(nome, TipoCampo.valueOf(tipo), inizio, fine, note));
                }
            }

            addTipoRecord(tipiRecord, codTipoRecord, campi, recordSize);
        }

        return new MetaMovago(U.asUnmodifiableMap(tipiRecord), recordSize);
    }

    private static void printRecords(final Path fileDaVerificare, final MetaMovago metaMovago)
            throws IOException {

        try (@SuppressWarnings("null")
        final Reader in = U.newBufferedReader(fileDaVerificare, StandardCharsets.ISO_8859_1)) {

            // Possiamo riusare buf perché viene copiato in una String.
            final char[] buf = new char[metaMovago.recordSize];

            do {
                boolean trovatoQualcosa = false;

                final String record = readRecord(in, buf);

                if (record.isEmpty())
                    break;

                for (final Map.Entry<String, TipoRecord> t : metaMovago.tipiRecord.entrySet()) {
                    @SuppressWarnings("null")
                    final TipoRecord tipoRecord = t.getValue();
                    if (tipoRecord.dataIsRecordOfThisType(record)) {
                        tipoRecord.print(record);
                        trovatoQualcosa = true;
                    }
                }

                if (!trovatoQualcosa)
                    System.err.println("Record di tipo sconosciuto: " + record);

            } while (true);
        }
    }

    /**
     * Legge buf.length caratteri da in. Se ne sono disponibili di meno, solleva
     * un'eccezione {@link IllegalStateException}.
     *
     * @param in  Da dove leggere i caratteri.
     * @param buf Buffer di appoggio che fornisce anche il numero di caratteri da
     *            leggere.
     * @return Una stringa vuota se non ci sono più caratteri da leggere. Una
     *         stringa di buf.length caratteri letti da in.
     * @throws IOException           Per errori di IO su in.
     * @throws IllegalStateException Se non si riescono a leggere buf.length
     *                               caratteri pur non essendo esaurito l'input.
     */
    private static String readRecord(final Reader in, final char[] buf) throws IOException {
        final int count = in.read(buf, 0, buf.length);

        if (count == -1)
            // Non ci sono più record
            return "";

        if (count != buf.length)
            // Il record letto è incompleto
            throw new IllegalStateException("Richiesti " + buf.length + ", letti " + count);

        @SuppressWarnings("null")
        @NonNull
        final String record = String.valueOf(buf);

        return record;
    }

    public static void main(final String[] args) throws Exception {
        final Path csvTipiRecord = U.getPath("MOVAGO-records500.csv");
        final Path fileDaVerificare = U.getPath("MOVAGO-3.txt");

        final MetaMovago metaMovago = fromCsv(csvTipiRecord);

        printRecords(fileDaVerificare, metaMovago);
    }
}
