package it.nuccioservizi.welcome2any.movago;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

import it.nuccioservizi.welcome2any.Props;

public final class Cfg {

    public final String filtro_nomi_file_welcome;
    public final String suffisso_num_documento_fattura;
    public final String suffisso_num_documento_nota_di_credito;
    public final String suffisso_num_documento_fattura_di_locazione;
    public final String suffisso_num_documento_ricevuta_fiscale;
    public final String causale_welcome_fattura;
    public final String causale_welcome_nota_di_credito;
    public final String causale_welcome_ricevuta_fiscale;

    public final int cod_ditta;
    public final String cod_applicazione;
    public final String causale_contabile_fattura_vendita;
    public final String causale_contabile_corrispettivo;
    public final String causale_contabile_nota_di_credito_vendita;
    public final String causale_contabile_prima_nota_generica;
    public final String conto_clienti_corrispettivi_da_incassare;
    public final String conto_mastro_clienti;

    public Cfg(final Properties props) {
        // @formatter:off
        filtro_nomi_file_welcome = Props.getString(props, "filtro_nomi_file_welcome");
        suffisso_num_documento_fattura = Props.getString(props, "suffisso_num_documento_fattura");
        suffisso_num_documento_nota_di_credito = Props.getString(props, "suffisso_num_documento_nota_di_credito");
        suffisso_num_documento_fattura_di_locazione = Props.getString(props, "suffisso_num_documento_fattura_di_locazione");
        suffisso_num_documento_ricevuta_fiscale = Props.getString(props, "suffisso_num_documento_ricevuta_fiscale");
        causale_welcome_fattura = Props.getString(props, "causale_welcome_fattura");
        causale_welcome_nota_di_credito = Props.getString(props, "causale_welcome_nota_di_credito");
        causale_welcome_ricevuta_fiscale = Props.getString(props, "causale_welcome_ricevuta_fiscale");

        cod_ditta = Props.getInt(props, "cod_ditta");
        cod_applicazione = Props.getString(props, "cod_applicazione");
        causale_contabile_fattura_vendita = Props.getString(props, "causale_contabile_fattura_vendita");
        causale_contabile_corrispettivo = Props.getString(props, "causale_contabile_corrispettivo");
        causale_contabile_nota_di_credito_vendita = Props.getString(props, "causale_contabile_nota_di_credito_vendita");
        causale_contabile_prima_nota_generica = Props.getString(props, "causale_contabile_prima_nota_generica");
        conto_clienti_corrispettivi_da_incassare = Props.getString(props, "conto_clienti_corrispettivi_da_incassare");
        conto_mastro_clienti = Props.getString(props, "conto_mastro_clienti");
        // @formatter:on
    }

    public static Cfg load(final Path propsPath) throws IOException {
        return new Cfg(Props.load(propsPath));
    }
}
