package it.nuccioservizi.welcome2any.movago;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.annotation.NonNull;

import it.nuccioservizi.welcome2any.U;

public enum TipoCampo {
    CHR {
        @Override
        public String print(final String valore, final int lunghezza) {
            if (valore.length() >= lunghezza) {
                @SuppressWarnings("null")
                @NonNull
                final String s = valore.substring(0, lunghezza);
                return s;
            }
            return U.format("%-" + lunghezza + "s", valore);
        }

    },
    NUM {
        @SuppressWarnings("null")
        private final Pattern NUM_PATTERN = Pattern.compile("\\A0*([1-9][0-9]*)\\z");

        @Override
        public String print(final String valore, final int lunghezza) {
            Validate.isTrue(
                    valore.length() <= lunghezza, "Numero troppo grande, compo di %d cifre: %s",
                    Integer.valueOf(lunghezza), valore
            );

            final Matcher m = NUM_PATTERN.matcher(valore);
            if (m.matches())
                return U.format("%0" + lunghezza + "d", Long.valueOf(m.group(1)));

            return U.format("%" + lunghezza + "s", "");
        }

        @Override
        public String print(final int valore, final int lunghezza) {
            final String s = Integer.toString(valore);

            return print(Validate.notNull(s), lunghezza);
        }

    },
    FILLER {
        @Override
        public String print(final String valore, final int lunghezza) {
            return U.format("%" + lunghezza + "s", "");
        }

    },
    D {
        @SuppressWarnings("null")
        private final DateTimeFormatter FORMATTER = DateTimeFormatter.BASIC_ISO_DATE;
        private final String NESSUN_GIORNO = "        ";

        private String format(final LocalDate data) {
            final String s = data.format(FORMATTER);
            Validate.notNull(s);
            return s;
        }

        @Override
        public String print(final String valore, final int lunghezza) {
            if (valore.trim().isEmpty())
                return NESSUN_GIORNO;

            try {
                LocalDate.parse(valore, FORMATTER);
            } catch (final DateTimeParseException ex) {
                throw new IllegalArgumentException(
                        String.format("'%s' non è una data valida.", valore), ex
                );
            }

            return valore;
        }

        @Override
        public String print(final Optional<LocalDate> data) {
            return data.isPresent() ? format(data.get()) : NESSUN_GIORNO;
        }
    };

    public abstract String print(String valore, int lunghezza);

    @SuppressWarnings("static-method")
    public String print(
            @SuppressWarnings("unused") final Optional<LocalDate> data) {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("static-method")
    public String print(@SuppressWarnings("unused") final int numero, @SuppressWarnings(
        "unused"
    ) final int lunghezza) {
        throw new UnsupportedOperationException();
    }
}
