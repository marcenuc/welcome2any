package it.nuccioservizi.welcome2any.movago;

import org.eclipse.jdt.annotation.Nullable;

final class Campo implements Comparable<Campo> {
    final String nome;
    final TipoCampo tipo;
    final int inizio;
    final int fine;
    final int lunghezza;
    final String note;

    Campo(final String nome, final TipoCampo tipo, final int inizio,
            final int fine, final String note) {
        this.nome = nome;
        this.tipo = tipo;
        this.inizio = inizio;
        this.fine = fine;
        this.lunghezza = fine - inizio + 1;
        this.note = note;
    }

    @Override
    public String toString() {
        return "Campo [nome=" + this.nome + ", tipo=" + this.tipo + ", inizio="
                + this.inizio + ", fine=" + this.fine + ", lunghezza="
                + this.lunghezza + ", note=" + this.note + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.fine;
        result = prime * result + this.inizio;
        result = prime * result + this.lunghezza;
        result = prime * result + this.nome.hashCode();
        result = prime * result + this.note.hashCode();
        result = prime * result + this.tipo.hashCode();
        return result;
    }

    @Override
    public boolean equals(final @Nullable Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Campo other = (Campo) obj;
        if (this.fine != other.fine)
            return false;
        if (this.inizio != other.inizio)
            return false;
        if (this.lunghezza != other.lunghezza)
            return false;
        if (!this.nome.equals(other.nome))
            return false;
        if (!this.note.equals(other.note))
            return false;
        if (this.tipo != other.tipo)
            return false;
        return true;
    }

	@Override
	public int compareTo(Campo other) {
        return this.inizio < other.inizio //
                ? -1 //
                : this.inizio > other.inizio //
                        ? 1 //
                        : this.fine > other.fine //
                                ? -1 //
                                : this.fine < other.fine //
                                        ? 1 //
                                        : this.nome.compareTo(other.nome);
	}

}
