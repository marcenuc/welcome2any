# Welcome2G1

Se usi il gestionale Welcome di Passepartout per la tua azienda e segui la
contabilità con Gestionale1 di Zucchetti, con questo programmino puoi convertire
i file generati da Welcome in file importabili da Gestionale1. In questo modo
la contabilità può essere aggiornata rapidamente senza dover reinserire a mano
le anagrafiche dei clienti, le fatture, le note di credito, i pagamenti, le
prime note, le caparre.

Al momento l'implementazione è molto grezza, ma è in uso in due realtà e
funziona.

Per compilare il progetto è sufficiente usare i comandi standard di Apache
Maven.

## Inserimento dell'anagrafica clienti

Il programma cerca nel file ANACF.DBF di Gestionale1 i clienti già presenti in
contabilità per evitare di creare duplicati. La ricerca viene fatta per codice
fiscale e partita IVA, ma anche per codice cliente di Welcome che viene salvato
nel campo ALIAS di ANACF.DBF. Questo permette di evitare duplicati anche per
clienti stranieri per i quali CF e PIVA non sono presenti.
